import asyncio

import websockets
import json

from backend.main import *
from backend.detection import *
from backend.pointscalculator import *

class ChatServerImpl(ChatServer):
    async def announce(self, message_id: str, rating: MessageRating, websocket) -> None:
        global detoxHandler
        global chat
        chat.append((users[websocket]+": "+detoxHandler.get_message_text(message_id),rating.message_severity * rating.detection_accuracy))
        print(rating.message_severity * rating.detection_accuracy)
        response = { "type":"send", "text":users[websocket]+": "+detoxHandler.get_message_text(message_id), "score":rating.message_severity * rating.detection_accuracy }
        await broadCast(response)

    async def warn_toxic(self, message_id: str, rating: MessageRating, websocket) -> None:
        global badmessages
        print(detoxHandler.get_message_text(message_id))
        badmessages[websocket] = (detoxHandler.get_message_text(message_id), message_id)
        response = { "type":"warn", "score":rating.message_severity * rating.detection_accuracy, "text":detoxHandler.get_message_text(message_id)}
        await sendMessage(websocket,response)


ami = MessageInspectorV3()
chatserver = ChatServerImpl()
detoxHandler = DetoxHandler(ami,chatserver)


chat = []
users = {}
mutedUsers = {}

badmessages = {}

async def refreshChat(websocketuser):
    global chat
    global mutedUsers
    filteredHistory = []
    usernames = []
    for x in chat:
        if x[0].split(":")[0] not in mutedUsers[users[websocketuser]]:
            filteredHistory.append(x)
            usernames.append(x[0].split(":")[0])

    chatHistory = { "type":"history", "text":[x[0] for x in filteredHistory], "username":usernames, "score":[x[1] for x in filteredHistory] }
    await sendMessage(websocketuser,chatHistory)

async def broadCast(msg):
    global users
    newusers = {}
    for connection in users:
        try:
            await sendMessage(connection,msg)
            newusers[connection] = users[connection]
        finally:
            pass

async def handler(websocket):
    global users
    global badmessages

    try:
        async for message in websocket:
            try:
                print(message)
                
                event = json.loads(message)
                
                if event["type"] == "leave":
                    print("User", users[websocket], " left!")
                    if websocket in users:
                        del users[websocket]
                        ulist = { "type":"userUpdate", "userlist":list(users.values()) }
                        await broadCast(ulist)
                
                if event["type"] == "ignore":
                    print(event)
                    await detoxHandler.ignore_warning(badmessages[websocket][1])

                if event["type"] == "join":
                    if websocket in users:
                        pass
                    
                    userID = 0
                    while True:
                        havetoAdd = 0
                        for x in users:
                            if users[x] == "User" + str(userID):
                                userID += 1
                                havetoAdd = 1
                                break
                        if not havetoAdd:
                            break
                    users[websocket] = "User" + str(userID)

                    if users[websocket] not in mutedUsers:
                        mutedUsers[users[websocket]] = []

                    print("User", users[websocket], " joined!", mutedUsers[users[websocket]])

                    await refreshChat(websocket)

                    regist = { "type":"register", "name":users[websocket] }
                    ulist = { "type":"userUpdate", "userlist":list(users.values()) }

                    await sendMessage(websocket,regist)
                    await broadCast(ulist)

                    mutedUserData = {"type":"mutedUsers", "users":mutedUsers[users[websocket]]}
                    await sendMessage(websocket,mutedUserData)
                    
                
                if event["type"] == "send":

                    try:
                        if websocket in badmessages:
                            for msgIdId in badmessages[websocket]:
                                try:
                                    detoxHandler.unregister_message(msgIdId[1])
                                except Exception as exc:
                                    pass
                        msgID = detoxHandler.register_message(event["text"], websocket)
                        await detoxHandler.send_message(msgID)
                    except Exception as e:
                        print(e)

                if event["type"] == "mute":
                    if event["username"] != users[websocket]:
                        print(users[websocket],"muted",event["username"])
                        if event["username"] not in mutedUsers[users[websocket]]:
                            mutedUsers[users[websocket]].append(event["username"])
                            await refreshChat(websocket)
                            mutedUserData = {"type":"mutedUsers", "users":mutedUsers[users[websocket]]}
                            await sendMessage(websocket,mutedUserData)
                
                if event["type"] == "unmute":
                    if event["username"] != users[websocket]:
                        print(users[websocket],"unmuted",event["username"])
                        if event["username"] in mutedUsers[users[websocket]]:
                            mutedUsers[users[websocket]].remove(event["username"])
                            await refreshChat(websocket)
                            mutedUserData = {"type":"mutedUsers", "users":mutedUsers[users[websocket]]}
                            await sendMessage(websocket,mutedUserData)


                if event["type"] == "flag":
                    if event["username"] != users[websocket]:
                        print(users[websocket],"flagged",event["username"])

            except Exception as e:
                print("Excteption occured:",e)
                print("failed on", websocket)
                if websocket in users:
                    del users[websocket]
                    ulist = { "type":"userUpdate", "userlist":list(users.values()) }
                    await broadCast(ulist)

    except Exception as ex:
        print(ex)
        print("failed on", websocket)
        if websocket in users:
            del users[websocket]
            ulist = { "type":"userUpdate", "userlist":list(users.values()) }
            await broadCast(ulist)

async def main():
    async with websockets.serve(handler, "", 8001):
        await asyncio.Future()


async def sendMessage(websocketuser, msg):
    try:
        await websocketuser.send(json.dumps(msg))
    except Exception as ex:
        print(ex)
        print("trying to reconnect")
        try:
            await websocketuser.connect("ws://detox.chat:8001/")
            await websocketuser.send(json.dumps(msg))
        except:
            print("FAILED")
            print("connection timed out")
            if websocketuser in users:
                    del users[websocketuser]
                    ulist = { "type":"userUpdate", "userlist":list(users.values()) }
                    await broadCast(ulist)

if __name__ == "__main__":
    asyncio.run(main())